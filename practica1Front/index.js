/**
 * Servidor Node JS con Express Ejecutando en puerto 3005
 */
const express = require('express');
const cors = require('cors');
const path = require('path');
require('dotenv').config();

const app = express();
app.use(cors());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({extended: true, limit: '50mb'}));

const server = require('http').createServer(app);

const port = process.env.PORT;

const publicPath = path.join(__dirname, 'public');
app.use(express.static(publicPath));

server.listen(port, (err) => {
    if (err) {
        throw new Error(err);
    }

    console.log(`Server ejecutando en http://localhost:${port}`);
});
